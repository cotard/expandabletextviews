//
//  ExpandableTextViewsViewController.m
//  ExpandableTextViews
//
//  Created by Maria Patani on 25.11.15.
//  Copyright © 2015 Maria Patani. All rights reserved.
//

#import "ExpandableTextViewsViewController.h"

typedef NS_ENUM(NSUInteger, TextViewTag) {
    TextViewTagFirst,
    TextViewTagSecond,
    TextViewTagThird,
    TextViewTagFourth
};

static CGFloat const kTextViewVerticalMarginValue = 15.0;

@interface ExpandableTextViewsViewController () <UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *viewContainer;

@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *textViewsHeightsCollection;
@property (strong, nonatomic) IBOutletCollection(UITextView) NSArray *textViewsCollection;

@property (strong, nonatomic) UITextView *activeTextView;
@property (nonatomic) CGFloat originalTextViewHeight;

@end

@implementation ExpandableTextViewsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self addTapGesture];
    [self addToolbarForKeyboard];
    [self setTextViewTags];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self registerForKeyboardNotifications];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setTextViewTags
{
    for (TextViewTag tag = TextViewTagFirst; tag < TextViewTagFourth; tag++) {
        UITextView *textView = self.textViewsCollection[tag];
        textView.tag = tag;
    }
}

- (void)addTapGesture
{
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundClicked)];
    [self.viewContainer addGestureRecognizer:tapRecognizer];
}

- (void)backgroundClicked
{
    if (!self.activeTextView) {
        return;
    }
    
    [self setHeightConstraintForActiveTextView:self.originalTextViewHeight];
    [self.view endEditing:YES];
}

- (void)doneToolbarButtonClicked
{
    if (!self.activeTextView) {
        return;
    }
    
    CGFloat fixedWidth = CGRectGetWidth(self.activeTextView.frame);
    CGSize newTextViewSize = [self.activeTextView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    [self setHeightConstraintForActiveTextView:newTextViewSize.height];
    
    [self.view endEditing:YES];
}

- (void)addToolbarForKeyboard
{
    UIToolbar *keyboardToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 50.0)];
    
    NSString *cancelLocalizedString = NSLocalizedString(@"Cancel", @"Cancel bar button title in the expandable textviews controller");
    NSString *doneLocalizedString = NSLocalizedString(@"Done", @"Done bar button title in the expandable textviews controller");
    
    keyboardToolbar.items = [NSArray arrayWithObjects:
                             [[UIBarButtonItem alloc]initWithTitle:cancelLocalizedString
                                                             style:UIBarButtonItemStylePlain
                                                            target:self
                                                            action:@selector(backgroundClicked)],
                             [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                          target:nil
                                                                          action:nil],
                             [[UIBarButtonItem alloc] initWithTitle:doneLocalizedString
                                                              style:UIBarButtonItemStyleDone
                                                             target:self
                                                             action:@selector(doneToolbarButtonClicked)],
                             nil];
    [keyboardToolbar sizeToFit];
    
    for (UITextView *textView in self.textViewsCollection) {
        textView.inputAccessoryView = keyboardToolbar;
    }
}

- (void)setHeightConstraintForActiveTextView:(CGFloat)constraintValue
{
    for (int i = 0; i < self.textViewsHeightsCollection.count; i++) {
        if (self.activeTextView.tag == i) {
            NSLayoutConstraint *constraint = self.textViewsHeightsCollection[i];
            self.originalTextViewHeight = constraint.constant;
            constraint.constant = constraintValue;
            break;
        }
    }
}

#pragma mark - Notifications

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification *)notification
{
    NSDictionary *info = [notification userInfo];
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.scrollView.frame.origin.y, 0, keyboardSize.height, 0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    CGFloat heightForActiveTextView = CGRectGetHeight(self.view.frame) - keyboardSize.height - contentInsets.top - kTextViewVerticalMarginValue * 2;
    [self setHeightConstraintForActiveTextView:heightForActiveTextView];
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
        CGFloat yScrollPosition = self.activeTextView.frame.origin.y - kTextViewVerticalMarginValue;
        [self.scrollView setContentOffset:CGPointMake(0, yScrollPosition)];
    }];
}

- (void)keyboardWillBeHidden:(NSNotification *)notification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

#pragma mark - UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    self.activeTextView = textView;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self.scrollView scrollRectToVisible:self.activeTextView.frame
                                    animated:YES];
        self.activeTextView = nil;
    }];
}

@end
