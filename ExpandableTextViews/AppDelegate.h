//
//  AppDelegate.h
//  ExpandableTextViews
//
//  Created by Maria Patani on 25.11.15.
//  Copyright © 2015 Maria Patani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

